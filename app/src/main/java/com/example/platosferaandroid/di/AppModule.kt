package com.example.platosferaandroid.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * Created by Dmitriy Tomilov on 04/04/2019.
 */

@Module
class AppModule {
    @Provides
    fun provideApplicationContext(application: Application): Context = application
}