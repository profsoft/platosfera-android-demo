package com.example.platosferaandroid.di

import com.example.platosferaandroid.mvp.data.network.IAuthApiService
import com.example.platosferaandroid.mvp.data.repository.AuthRepository
import com.example.platosferaandroid.mvp.data.repository.IAuthRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoreModule {
    @Provides
    fun provideAuthRepository(
        authApiService: IAuthApiService
    ): IAuthRepository = AuthRepository(authApiService)
}