package com.example.platosferaandroid.di

import android.app.Application
import com.example.platosferaandroid.PlatosferaApplication
import com.example.platosferaandroid.mvp.ui.login.LoginActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, PresenterModule::class, RepositoreModule::class])
interface AppComponent {

    fun inject(application:PlatosferaApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    interface ComponentProvider {
        val appComponent: AppComponent
    }

    fun inject(activity: LoginActivity)

}