package com.example.platosferaandroid.di

import com.example.platosferaandroid.mvp.data.repository.AuthRepository
import com.example.platosferaandroid.mvp.presentation.login.LoginPresenter
import dagger.Module
import dagger.Provides

@Module
class PresenterModule {
    @Provides
    fun provideLoginPresenter(authRepository: AuthRepository) = LoginPresenter(authRepository)
}