package com.example.platosferaandroid.mvp.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("name") var firstName: String? = null,
    @SerializedName("surname") val secondName: String? = null,
    @SerializedName("cityName") val cityName: String? = null,
    @SerializedName("birthDate") val birthDate: Long? = null,
    @SerializedName("nickName") val nickname: String? = null,
    @SerializedName("gender") val gender: Int? = null,
    @SerializedName("email") val email: String? = null,
    @SerializedName("password") val password: String? = null,
    @SerializedName("phone") var phone: String? = null,
    @SerializedName("status") val status: String? = null,
    @SerializedName("registrationStatus") val registrationStatus: Int? = null
) : Parcelable