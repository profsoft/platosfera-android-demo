package com.example.platosferaandroid.mvp.data.entity

import com.google.gson.annotations.SerializedName

data class RegistrationResponse(
        @SerializedName("createdAt") val createdAt: Int?,
        @SerializedName("platform") val platform: String?,
        @SerializedName("status") val status: String?,
        @SerializedName("user") val user: User?
)