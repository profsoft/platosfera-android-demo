package com.example.platosferaandroid.mvp.ui.login

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.platosferaandroid.PlatosferaApplication
import com.example.platosferaandroid.R
import com.example.platosferaandroid.mvp.presentation.login.ILoginView
import com.example.platosferaandroid.mvp.presentation.login.LoginPresenter
import com.example.platosferaandroid.mvp.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : MvpAppCompatActivity(), ILoginView {


    init {
        PlatosferaApplication.INSTANCE.appComponent.inject(this@LoginActivity)
    }

    @Inject
    @InjectPresenter
    lateinit var presenter: LoginPresenter

    @ProvidePresenter
    fun providePresenter(): LoginPresenter = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_start.setOnClickListener {
            //presenter.login(email.text?.toString()?:"", password_edit.text?.toString()?:"")
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun routeToConfirmCode() {
        //переход к экрану подтверждения кода
    }

    override fun routeToRegistrationData() {
        //переход к вводу личных данных
    }

    override fun startMain() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun showErrorEmptyEmail() {
        //какая то логика связанная с пустым имеилом
    }

    override fun showErrorEmptyPassword() {
        //какая то логика связанная с пустым паролем
    }

    override fun showDialog() {
        //показать экран загрузки
    }

    override fun hideDialog() {
        //скрыть экран загрузки
    }

    override fun showMessage(message: String) {
        //метод показывающий ошибку
    }
}
