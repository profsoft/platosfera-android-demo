package com.example.platosferaandroid.mvp.presentation.login

import com.arellomobile.mvp.InjectViewState
import com.example.platosferaandroid.mvp.data.constants.RegistrationConst
import com.example.platosferaandroid.mvp.data.entity.LoginRequest
import com.example.platosferaandroid.mvp.data.repository.AuthRepository
import com.example.platosferaandroid.mvp.presentation.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers

@InjectViewState
class LoginPresenter(
    private val authRepository: AuthRepository
) :BasePresenter<ILoginView>() {

    fun login(email: String, password: String) {
        when (validate(email, password)) {
            true -> {
                authRepository.login(LoginRequest(email, password))
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { viewState.showDialog() }
                    .doAfterTerminate { viewState.hideDialog() }
                    .subscribe({ auth ->
                        when(auth.user?.registrationStatus){
                            RegistrationConst.NOT_CONFIRM_EMAIL -> {
                                viewState.routeToConfirmCode()
                            }
                            RegistrationConst.EMPTY_DATA -> {
                                viewState.routeToRegistrationData()
                            }
                            RegistrationConst.SUCCESS -> {
                                viewState.startMain()
                            }
                            else -> throw IllegalArgumentException("invalid error")
                        }
                    }, {
                        it.message?.let { error -> viewState.showMessage(error) }
                    }).connect()
            }
            false -> return
        }
    }

    private fun validate(email: String, password: String): Boolean {
        if (email.isEmpty() && password.isEmpty()) {
            viewState.showErrorEmptyEmail()
            viewState.showErrorEmptyPassword()
            return false
        } else if (email.isEmpty()) {
            viewState.showErrorEmptyEmail()
            return false
        } else if (password.isEmpty()) {
            viewState.showErrorEmptyPassword()
            return false
        }
        return true
    }

}