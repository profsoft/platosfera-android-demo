package com.example.platosferaandroid.mvp.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginResponse(
    @SerializedName("token") val token: String,
    @SerializedName("user") val user: User?
): Parcelable