package com.example.platosferaandroid.mvp.presentation.login

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface ILoginView : MvpView{

    fun routeToConfirmCode()

    fun routeToRegistrationData()

    fun startMain()

    fun showErrorEmptyEmail()

    fun showErrorEmptyPassword()

    fun showDialog()

    fun hideDialog()

    fun showMessage(message : String)
}