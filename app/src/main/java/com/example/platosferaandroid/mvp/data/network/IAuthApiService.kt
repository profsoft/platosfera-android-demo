package com.example.platosferaandroid.mvp.data.network

import com.example.platosferaandroid.mvp.data.entity.LoginRequest
import com.example.platosferaandroid.mvp.data.entity.LoginResponse
import com.example.platosferaandroid.mvp.data.entity.RegistrationRequest
import com.example.platosferaandroid.mvp.data.entity.RegistrationResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface IAuthApiService {

    @POST(".../...")
    fun login(@Body request : LoginRequest) : Single<LoginResponse>

    @POST(".../...")
    fun registration(
        @Body request: RegistrationRequest,
        @Query("lang", encoded = false) locale: String
    ) : Single<RegistrationResponse>

}