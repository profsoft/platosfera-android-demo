package com.example.platosferaandroid.mvp.data.constants

object RegistrationConst {
    const val NOT_CONFIRM_EMAIL = 0
    const val EMPTY_DATA = 1
    const val SUCCESS = 2
}