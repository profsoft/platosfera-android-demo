package com.example.platosferaandroid.mvp.data.repository

import com.example.platosferaandroid.mvp.data.entity.LoginRequest
import com.example.platosferaandroid.mvp.data.entity.LoginResponse
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import com.example.platosferaandroid.mvp.data.entity.RegistrationRequest
import com.example.platosferaandroid.mvp.data.entity.RegistrationResponse
import com.example.platosferaandroid.mvp.data.network.IAuthApiService
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val authApiService: IAuthApiService
) : IAuthRepository {

    override fun login(login: LoginRequest): Single<LoginResponse> =
        authApiService.login(login)
            .subscribeOn(Schedulers.io())

    override fun registration(
        request: RegistrationRequest,
        locale: String
    ): Single<RegistrationResponse> =
        authApiService.registration(request, locale)
            .subscribeOn(Schedulers.io())

}