package com.example.platosferaandroid.mvp.data.repository

import com.example.platosferaandroid.mvp.data.entity.LoginRequest
import com.example.platosferaandroid.mvp.data.entity.LoginResponse
import com.example.platosferaandroid.mvp.data.entity.RegistrationRequest
import com.example.platosferaandroid.mvp.data.entity.RegistrationResponse
import io.reactivex.Single

interface IAuthRepository {

    fun login(login: LoginRequest) : Single<LoginResponse>

    fun registration(request: RegistrationRequest, locale: String) : Single<RegistrationResponse>
}