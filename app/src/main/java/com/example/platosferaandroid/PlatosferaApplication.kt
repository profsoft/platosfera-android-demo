package com.example.platosferaandroid

import android.app.Application
import com.example.platosferaandroid.di.AppComponent
import com.example.platosferaandroid.di.DaggerAppComponent


class PlatosferaApplication : Application(), AppComponent.ComponentProvider {

    override lateinit var appComponent: AppComponent
    companion object {
        lateinit var INSTANCE: PlatosferaApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
        appComponent.inject(this)
    }
}